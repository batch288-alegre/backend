// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here
for(let index = 0 ; index < string.length; index++ ){

	if(string[index] === "a" ||
		string[index] === "e" ||
		string[index] === "i" ||
		string[index] === "o" ||
		string[index] === "u"){
		continue;
	}else{
		filteredString += string[index];
	}

}
	console.log(filteredString);






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}