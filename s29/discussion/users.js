// Advance Queries

db.users.find({
	contact:{
		email: "stephenhawking@gmail.com"
	}
})

// Dot notation

db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
})

// querying an array with exact element

db.users.find({
	courses : ["CSS", "Javascript", "Python"]
})

db.users.find({courses : {
	$all: ["React"]
	}
}
)

db.users.find({courses : {
	$all: ["React","Laravel"]
	}
}
)

db.users.find({courses : {
	$all: ["React","Sass"]
	}
}
)

// Query Operators

// [Section] Comparison Query Operators
	// $gt/gte operator

	/*
		It allows us to find documents that have field number values greater or equal to a specified value
		-Note that this operator will only work if the data type of the field is number or integer.
		db.collectionName.find({field : {
		$gt: value
		}})

		db.collectionName.find({field : {$gte: value}})
	*/

db.users.find({age : {
	$gt: 76,
}})

db.users.find({age : {$gte: 76,}})

	// $lt/lte operator
	/*
		-it allows us to find documents that have field number less than or equal to a specified value
		- Note : same with the $gt/gte operator, this will only work if the data type of the field being queried is number or Int.
		Syntax:
		db.collectionName.find({field : {
		$lt: value
		}})
		db.collectionName.find({field : {$lte: value}})
	*/

db.users.find({age : {
	$lt: 76,
}})

db.users.find({age : {$lte: 76,}})
	
	// $ne operator
	/*
		-allows us to find documents that have field number values not equal to a specified value
		Syntax:
		db.collectionName.find({field : {$ne: value}})
	*/

db.users.find({age : {$ne: 76,}})

	// $in operator
	/*
		-allows us to find documents with specific match criteria one field using different values
		Syntax:
		db.collectionName.find({criteria.field : {$in : value}})
	*/
	
db.users.find({lastName: {$in : ["Hawking", "Doe", "Armstrong"]}})
db.users.find({"contact.phone": {$in : ["87654321"]}})

// Using in operator in and array
db.users.find({courses: {$in : ["React"]}})
db.users.find({courses: {$in : ["React", "Laravel"]}})

db.users.find({lastName: "Hawking"})
db.users.find({lastName: "Doe"})

// [Section] Logical Query Operators
	// $or operator
	/*
		-allow us to find documents that match a single criteria from multiple provided search criteria
		Syntax :
			db.collectionName.find({
				$or :
				[
					{fieldA : valueA},
					{fieldB : valueB}
				]
			})
	*/

db.users.find({ $or : [{firstName : "Neil"}, {age : 25}]})

// add multiple operatos
db.users.find({$or :[{firstName : "Neil"}, {age : {$gt : 25}}]})

db.users.find({
	$or :
	[
		{firstName : "Neil"},
	{age : {$gt : 25}},
	{courses: {$in : ["HTML"]}}
]})

	// $and operator
	/*
		-allows us to find documents matching all the multiple criteria in a single field.
		Syntax :
			db.collectionName.find({
				$and :
				[
					{fieldA : valueA},
					{fieldB : valueB}
				]
			})
	*/

db.users.find({
				$and :
				[
					{age : {$ne : 82}},
					{age : {$ne : 76}}
				]
			})
// Mini activity
	// You are going to query all the documents from the users collection, that follow these criteria:
		// Information of the documents older than 30 that is enrolled in CSS or HTML.
db.users.find({
	$and :
	[
		{age : {$gt: 30}},
		{courses :{$in: ["CSS", "HTML"]}}
	]
})

// [Section] Field Projection
	/*
		-retrieving documents are common operations tha we do and by default mongoDB queiries return the whole document as a response.
	*/
	// Inclusion
	/*
		-allows us to include or add specific fields only when retrieving documents:
		Syntax:
		db.collectName.find({criteria}, {field: 1});
	*/
	
	db.users.find(
		{firstName: "Jane"},
		{	
			_id : 0,
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	)

	db.users.find(
		{firstName: "Jane"},
		{"contact.phone" : 1}
	)

	// Exlusion
	/*
		allows us to exlude/remove specific fields only when retrieving documents
		Syntax
		db.collectionName.find({criteria}, {field: 0});
	*/

	db.users.find(
		{firstName: "Jane"},
		{contact : 0},
		{department : 0}
	)

	db.users.find(
		{firstName: "Jane"},
		{
			"contact.email" : 0,
			department : 0
	},
	
	)


	db.users.find(
	{$or:
	[{firstName : "Jane"},
		{age : {gte :30}}
		]
	},
	{"contact.email": 0,
		"department" : 0
	}
	)

	// Evaluation Query Operator
	// $regex operator
	/*
		-allows us to find documents that match a specific string pattern using regular expressions.
		syntax:
			db.collectionName.find({
								field : $regex: "pattern", option : "optionvalue"
			})
	*/
	// Case sensitive
	db.users.find({firstName: {$regex: "n"}});
	db.users.find({firstName: {$regex: "en"}});

	// Case insensitive
	db.users.find({firstName: {$regex: "n", $options: "i"}});
