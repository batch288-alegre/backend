// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

// Answer

	let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends: {
			home: ["May", "Max"],
			kanto:["Brock", "Misty"]
		},
		talk: function(){
			return (trainer.pokemon[0] + ", I choose you!")
		}

	}
	console.log(trainer);
	console.log("Result of dot notation: \n" +trainer.name);
	console.log("Result of square bracket notation: \n" +trainer["pokemon"]);
	console.log("Result of talk method: \n" +trainer.talk());

	function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		this.tackle = function(targetPokemon){
			targetPokemon.health = (targetPokemon.health - this.attack);
			console.log(this.name + " tackled " + targetPokemon.name)
			console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
			if(targetPokemon.health <= 0){
				this.faint(targetPokemon)
			}
		}
		this.faint = function(targetPokemon) {
			console.log(targetPokemon.name + " has fainted!");
	}


	}
	let pikachu = new Pokemon("Pikachu", 12,)
	let geodude = new Pokemon("Geodude", 8,)
	let mewtwo = new Pokemon("Mewtwo", 100,)

	Pokemon();

	console.log(pikachu);
	console.log(geodude);
	console.log(mewtwo);

// Geodude attack Pikachu
	geodude.tackle(pikachu)
	console.log(pikachu)

// Mewtwo attack Geodude
	mewtwo.tackle(geodude)
	console.log(geodude)







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}
