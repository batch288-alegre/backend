async function fruitsOnSale(db) {
	return await(

					db.fruits.aggregate([
							{$match : {onSale:true}},
							{$count : "total"}
						])

		);
};
async function fruitsInStock(db) {
	return await(

					db.fruits.aggregate([
							{$match : {stock : {$gte : 20}}},
							{$count : "total"}
						])

		);
};
async function fruitsAvePrice(db) {
	return await(

			db.fruits.aggregate([
							{$match : {onSale: true}},
							{$group : {
								_id : "$supplier_id",
								averagePrice : {$avg : {$sum : "$price"}}
								}},
						])


		);
};

async function fruitsHighPrice(db) {
	return await(

			db.fruits.aggregate([
					{$group : {
						_id: "$supplier_id",
						highestFruitPrice: {$max : "$price"}
					}},

				])

		);
};


async function fruitsLowPrice(db) {
	return await(

			db.fruits.aggregate([
					{$group : {
						_id: "$supplier_id",
						lowestFruitPrice: {$min : "$price"}
					}},

				])

		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};