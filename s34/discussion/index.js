// Use the "require" directive to load the express module /package
//It will allow us to access methods and functions that will help us create easily our application or server.
//A 'module' is a software component or part of a program that contains one or more routines.
const express = require("express");

//Create an application using express
//Creates an express applicaiton and stores this in a constant variable called app.
const app = express();

//For our application server to run, we nee a port ton listen
const port = 4000;


//Middlewares
	//is software that provides common services and capabilities to application outside of what's offere by the operating System.
	//Allow our application to read json data
	app.use(express.json());

	//This one will allow us to read data from forms.
	//By default, information received from the url can only be received a a string or an array.
	//By appluying the option of "extended : true " this will allow us to receive information in other  data types such as an object which will use throughout our application.
	app.use(express.urlencoded({extended: true}));

	//Section: Routes
	// Express has methods corresponding to each http method
	//This route expects to receive a GET request at the base URI "/"


	app.get("/", (request, response) => {
		//Once the route is accessed it will send a string response containing "Hello World!"
		//COmpared to the previous session, .end uses the node JS module's method
		//.send method uses the expressJS module's method instead to send a response back to the client.

		response.send('Hello Batch 288!');
	})

	

	//THis route expects to receive a GET request at the URI "/hello"
	app.get("/hello", (request, response)=>{
		response.send("Hello from the /hello endpoint");
	})

	// This route expects to receive a POST request at the URI "/hello"
	app.post("/hello", (request, response) => {
		//Request.body contains the contents/data of the request body
		//all the properties defined in our POSTMAN request will be accessible here as properties with the same name.
		console.log(request.body);

		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
	})

	//An array will store user objects when the "/signup" route is accessed
	//This will serve as our mock database
	let users = [];

	app.post("/signup", (request, response) => {
		/*console.log(request.body);*/
 		
 		if(request.body.username !== "" && request.body.password !== ""){

 			users.push(request.body);

 			response.send(`User ${request.body.username} successfully registered!`)

 		}else{
 			response.send('Please input BOTH username and password.');
 		}	
	})

	// This route expects to receive a PUT request at the URI "/change-password"
	//This will update the password of a user that matches the information provided in the client/postman
	app.put("/change-password", (request, response) => {

		let message;

			for(let index = 0 ; index < users.length; index++){
				
				//If the provided username in the client/postman and the username of the current object in the loop is the same
				if(request.body.username == users[index].username){
						
					users[index].password = request.body.password

					message = `User ${request.body.username}'s password has been updated!`
					break;
				}else{
					message = 'User does not exist!'
				}

			}
			response.send(message);
	})

	app.get("/home", (request,response) =>{
		response.send("Welcome to the home page");
	})

	app.get("/users", (request,response) =>{
		response.send(users)
	})

	app.delete("/delete-user", (request,response) => {
		let message;

		for(let index= 0; index < users.length; index++){
			if(request.body.username == users[index].username){
				users.splice(index,1);

				message = `User ${request.body.username} has been deleted.`
				break;
			};
		};
		if(message === undefined){
			message = "User does not exist."
		}else if(users.length == 0){
			message = "No users found."
		}
		response.send(message)
	})










	//Tell our server to listen to the port
	//if the port is accessed, we can run the server
	//return a message confirming that the server is running in the terminal


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
