const Users = require("../models/Users.js");
const bcrypt = require("bcrypt");

// Require the auth.js
const auth = require("../auth.js");

// Controllers
// Create a controller for the signup
// registerUser
/*Business Logic/Flow*/
	// 1. First, we have to validate whether the user is existing or not. We can do that by validating whether the email exist on our databases or not.
	// 2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	// 3. otherwise, we will sign up or add the user in our database.

module.exports.registerUser = (request,response) => {
		// find method: it will return an array of object that fits the given criteria

		Users.findOne({email : request.body.email})
		.then(result => {
			// we need to add if statement to verify whether the email already exist in our database
			if(result){
				return response.send(`${request.body.email} has already been taken! Try logging in or use different email in signing up!`)
			}else{
				// Create a new Object instantiated using the Users Model.
				let newUser = new Users({
					firstName: request.body.firstName,
					lastName: request.body.lastName,
					email: request.body.email,

					// hashSync method : it hash/encrypt our method
					// the second argument salt rounds
					password: bcrypt.hashSync(request.body.password, 10),
					mobileNo: request.body.mobileNo
				})

				// Save the user
				// Error handling
				newUser.save()
				.then(saved => response.send(`${request.body.email} is now registered`))
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error));
}

// new controller for the authentication of the user
module.exports.loginUser = (request,response) =>{

	Users.findOne({email: request.body.email})
	.then(result => {
		console.log(result)
		if(!result){
			return response.send(`${request.body.email} is not yet registered`)
		}else{
			console.log("Hi")

			// the compareSync method is used to compare a non encrypted password from the log in form to the encypted password retrieve from the find method. returns true or false depending on the result of the comparison
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password)
			if(isPasswordCorrect){
				return response.send({
					auth: auth.createAccessToken(result)
				})
			}else{
				return response.send("Please check your Password")
			}

		}
	})
	.catch(error => response.send(error));
}

module.exports.getProfile = (request,response) => {
	const id = request.body._id;
	Users.findById(id)
	.then(result => {
		password: result.password = "Confidential"
		return response.send(result);
	})
	.catch(error => response.send(error));

}