const jwt = require("jsonwebtoken")

// User defined string data that will be used to create our JSON web token
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined keyword.
const secret = "CourseBookingAPI"

// Section: JSON web token
// Json web token or JWT is a way of securely passing information from the server to the front end or the parts of server.
// Information is kept secure through the use of the secret cide.

// Function for token creation

// Analogy:
	/*
		Pack the gift and provide with the secret code as the key.
	*/

// the argument that will be passed in the parameter will be the document or object that contains the info of the user.

// get the object properties in your database in our case MongoDB Atlas in the collection
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}
	// Generate a JSOn web token using JWT's sign method.
	// generate the token using the form data and the secret code with no additional options provided.
	return jwt.sign(data, secret, {});
}
