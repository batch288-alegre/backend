const express = require("express")
const usersControllers = require("../controllers/usersControllers.js")

const router = express.Router();

// Routes

// route for registration
router.post("/register", usersControllers.registerUser)

// login
router.get("/login", usersControllers.loginUser)


// router for getting users details using users Id
router.get("/details",usersControllers.getProfile)

module.exports = router