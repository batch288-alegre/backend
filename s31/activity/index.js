const http = require("http");
const port = 3000;
const server = http.createServer(function (request, response){
	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type" : "text/plain"})

		response.end("You are in the Login Page")
	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"})

		response.end("Error 404 Page not found.")
	}

})

server.listen(port)

console.log(`Server ${port} is successfully running.`);