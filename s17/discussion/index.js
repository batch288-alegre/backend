// console.log("Hello World!");

//Functions
	// Functions in JavaScript are lines/blocks of codes that tell our device/applicatio to perform a certain task when called/ invoke.
	// Functions are mostly created to create complicated tasks to run several lines of code in succession.

// Function declarations
	// defines a function with the specified parameters
	// syntax:
		/*
			function functionName(){
				codeBlock/statements;
			}
		*/

	// function keyword - used to defined a JavaScript function
	//functionName - the function name. Functions are named to be able to use later in the code for the invocation
	// function block ({}) -  the statements which comprise the body of the function. This is where the code will be executed.

		//Example:
			function printName(){
				console.log("My name is John!");
			};
//Function Invocation
	// The code block and statements inside a function is not immediately executed when the function is defined. The codeblock and statements inside the function is executed when the function is invoked or called.
	// It is common to use the term "call a funcion" instead of "invoke a function".

			//Let's invoke/ call the function that we declare.
			printName();

// Function Declaration vs. Expression
	//Function Declaration
		// A function can be created through function declaration by busing the using the function keyword and adding the function name
		// Declared function/s can be hoisted. 

		declaredFunction();
		//Note: Hoistin is a JavaScript behavior for certain variables and functions to run or use before their declaration
		function declaredFunction(){
			console.log("Hello World from declaredFunction!")
		}

	//Function Expression
		//A function can also be stored in a variable.
		//A function expression is an anonymous function assigned to a varibale function.
		// variableFunction();

		let variableFunction = function(){
			console.log( "Hello Again!");
		};

		variableFunction();

		// We can also create a function expression of a named function.

		let funcExpression = function funcName(){
			console.log("Hello from the other side!")
		};

		funcExpression();

		//You can reassign declared functions and function expressions to new anonymous function.

		declaredFunction = function(){
			console.log("Updated declaredFunction!")
		};

		declaredFunction();

		funcExpression = function(){
			console.log("Updated funcExpression!");
		};
		funcExpression();

		// However, we cannot re-assign a function expression initialized with const.

		const constantFunc = function(){
			console.log("Initialized with const!");
			let x = 5;
		};
		constantFunc();

		/*constantFunc = function(){
			console.log("Cannot be re-assgined!");
		};

		constantFunc();*/


// Function Scoping
		// Scope it is the accessiblity of variables within our program.

		/*
			JavaScript Variables has 3 types of scope:
			1. Global
			2. local/block scope
			4. function scope
		*/

		{
			let localVar = "Armando Peres";


		}

		//Result in error. localVar, being in a block, it will not be accessible outside the block.
		/*console.log(localVar);*/


		let globalVar = "Mr. Worldwide";

		{
			console.log(globalVar);
		}

	//Function Scope
		// JavaScript has function scope: Each function creates a new scope.
		// Variables defined inside a function are not accessible (visible) from outside the function.

		function showNames(){
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionConst);
			console.log(functionLet);
		}

		/*console.log(functionConst);*/
		// console.log(functionLet);

		showNames();

	// Nested functions
		//You can create anothe function inside a function. This is called a nested function.

		function myNewFunction(){
			let name = "Jane";

			// nested function
			function nestedFunction(){
				console.log(name);
			}

			nestedFunction();
		}

		myNewFunction();

		let functionExpression = function(){

			function nestedFunction(){
				let greetings = "Hello Batch 288!";
				console.log(greetings);
			}

			nestedFunction();
		}

		functionExpression();

//Return Statement
		/*
			The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
		*/


		function returnFullName(){
			let fullName = "Jeffrey Smith Bezos";

			let returnArray = [1, 2, 3, 4, 5]

			return(returnArray);

		}

		returnFullName();



		console.log(returnFullName());


		let fullName = returnFullName();

		console.log(fullName);


// Function Naming Conventions
		//Function names should be definitive of the taks it will perform. It usually contains a verb.


		function getCourses(){
			let courses = ["Science 101", "Match 101", "English 101"];

			return course;
		}


		// Avoid generic names to avoid confusion within our code.
		function get(){
			let name = "Jamie";

			return name;
		}

		//Avoid pointless and inappropriate function names

		function foo(){

			return 25%5;
		}


		// Name your functions in small caps. Follo camelCame when naming variables and functions with multiple words.

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
		}


	//returning nested function

	function trialFunction(){

		function nestedFunction(){
			let name = "Lebron";

			return name;

		}
		console.log(nestedFunction());
		return nestedFunction();
	}



	console.log(trialFunction());
