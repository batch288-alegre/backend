const http = require("http");

http.createServer(function(request,response) {
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200,{"Content-Type" : "text/plain"})
		response.end("Data retrieved from the database");
	};
	if (request.url =="/items" && request.method == "POST"){
			response.writeHead(200,{"Content-Type" : "text/plain"})
			response.end("Data to be sent to the database")
	}

}).listen(4000)

console.log(`Server running at localhost: 4000`);

// (npx kill-port 4000) in git bash to kill active port