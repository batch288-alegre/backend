// console.log("Hello World!");
	
	function printInput(){
		let nickname = "Chris";

		console.log("Hi, " + nickname);
	}

	printInput();
	printInput();

	// For other cases, functions can also process data directly passed into it insted of relyingonly on Global Variables.

	// Consider this function:
		function printName(name){
		console.log("My name is " + name);
		}

		printName("Juana");

		printName();

	// variables can also be passed as an argument

		let sampleVariable = "Curry";

		let sampleArray = ["Davis", "Green", "Jokic", "Tatum"];

		printName(sampleVariable);
		printName(sampleArray);

	// One example of using the Parameter and Argument.
		//functionName(number);

	function checkDivisibilityBy8(num){
		let remainder = num % 8;

		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleBy8 = remainder === 0 ; 

		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);

	//Functions as Argument
		//Function parameters can also accept other functions as arguments.


	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed!")
	}


	function invokeFunction(func){
		func();
	}


	invokeFunction(argumentFunction);


	//Function as Argument with return keyword

	function returnFunction(){
		let name = "Chris";
	}


	function invokedFunction(func){
		console.log(func);
	}

	invokedFunction(returnFunction);

	// Using multiple parameters

	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}

	createFullName('Juan', 'Dela', 'Cruz');

	// In JavaScript, providing more/less arguments than the expected will not return an error


	// Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.
	createFullName('Gemar', null, 'Cruz');

	createFullName('Lito', "Masbate", "Galan", "Jr.");

	//In other programming languages, this will retrun an error stating that "the number of arguments do not match the number of parameters".

	//Using variable as Multiple arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	// Using alert()
	// alert("Hello World!"); //This will run immediately when the page loads
			// Syntax: alert("messageInString")

	// Alert inside a function

	function showSampleAlert(){
		alert ("Hello user!");
	}

	// showSampleAlert();

	// console.log("I will only log in the console when the alert is dismissed.");


	//Using Prompt()
			// prompt()allows us to show a small window at the top of the browser to gather user input
			// The value gathered from a prompt is retured as a string
			// Syntax: prompt("dialogInString")
	// let samplePrompt = prompt("Enter your name");
	// console.log	("Hello, "+ samplePrompt);
	// console.log(typeof samplePrompt);

	// let	age = prompt ("Enter your age: ");
	// console.log(typeof age);
	// console.log(age);

	// let sampleNullPrompt = prompt("Don't enter anything.");
	// console.log(sampleNullPrompt);

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name.");
		let lastName = prompt("Enter your last name.");

		console.log("Hello, " +firstName +" " +lastName	+ "!");
		console.log("Welcome to my page");
	}
	printWelcomeMessage();