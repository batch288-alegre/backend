const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "User first name is required!"]		
	},

	lastName: {
		type: String,
		required: [true, "User last name is required!"]		
	},

	email: {
		type: String,
		required: [true, "User email is required!"]		
	},

	password: {
		type: String,
		required: [true, "User password is required!"]		
	},

	isAdmin: {
		type: Boolean,
		default: false		
	},

	mobileNo: {
		type: String,
		required: [true, "User last name is required!"]		
	},

	enrollments: [
			{
				courseId: {
					type: String,
					required: [true, "Course Id of the enrollments is required"]
				},

				enrolledOn:{
					type: Date,
					default: new Date()
				},

				status: {
					type: String,
					default: "Enrolled"
				}
			}
		]
})

const Users = mongoose.model("User", userSchema);

module.exports = Users;