const jwt = require("jsonwebtoken")

// User defined string data that will be used to create our JSON web token
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined keyword.
const secret = "CourseBookingAPI"

// Section: JSON web token
// Json web token or JWT is a way of securely passing information from the server to the front end or the parts of server.
// Information is kept secure through the use of the secret cide.

// Function for token creation

// Analogy:
	/*
		Pack the gift and provide with the secret code as the key.
	*/

// the argument that will be passed in the parameter will be the document or object that contains the info of the user.

// get the object properties in your database in our case MongoDB Atlas in the collection
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}
	// Generate a JSOn web token using JWT's sign method.
	// generate the token using the form data and the secret 
	// code with no additional options provided.
	return jwt.sign(data, secret, {});
}

// Token Verification
/*
	Analogy:
		Received gift and open lock to verify if the sender is legitimate
*/
module.exports.verify = (request, response, next) =>{

	let token = request.headers.authorization;
	// console.log(token);

	// "Bearer "
	if (token !== undefined){
		token = token.slice(7, token.length);

		// Validate the token by using the "verify" method in decrypting the token using the secret code.
		return jwt.verify(token, secret, (error, data) =>{
			if (error){

				return response.send(false)
			}else{

				next();
			}
		})
	}else{
		return response.send(false)
	}
}

// decode the encypted token.
module.exports.decode = (token) => {
	token = token.slice(7, token.length);

	// The decode method is used to obtain the information from the JWT
	// The {complete: true} option allows us to return additional information from the JWT token

	return jwt.decode(token, {complete: true})
	.payload;

}