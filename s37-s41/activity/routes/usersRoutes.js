const express = require("express")
const usersControllers = require("../controllers/usersControllers.js")

const auth = require("../auth.js")
const router = express.Router();


// Routes

// route for registration
router.post("/register", usersControllers.registerUser)

// login
router.post("/login", usersControllers.loginUser)


// router for getProfile
router.get("/details", auth.verify, usersControllers.getProfile)

// route for course enrollment
router.post("/enroll", auth.verify, usersControllers.enrollCourse)

router.get("/userDetails", auth.verify, usersControllers.retriveUserDetails)

module.exports = router