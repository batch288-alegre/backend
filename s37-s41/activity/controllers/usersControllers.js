const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js")
const bcrypt = require("bcrypt");

// Require the auth.js
const auth = require("../auth.js");

// Controllers
// Create a controller for the signup
// registerUser
/*Business Logic/Flow*/
	// 1. First, we have to validate whether the user is existing or not. We can do that by validating whether the email exist on our databases or not.
	// 2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	// 3. otherwise, we will sign up or add the user in our database.

module.exports.registerUser = (request,response) => {
		// find method: it will return an array of object that fits the given criteria

		Users.findOne({email : request.body.email})
		.then(result => {
			// we need to add if statement to verify whether the email already exist in our database
			if(result){
				return response.send(false)
			}else{
				// Create a new Object instantiated using the Users Model.
				let newUser = new Users({
					firstName: request.body.firstName,
					lastName: request.body.lastName,
					email: request.body.email,

					// hashSync method : it hash/encrypt our method
					// the second argument salt rounds
					password: bcrypt.hashSync(request.body.password, 10),
					isAdmin: request.body.isAdmin,
					mobileNo: request.body.mobileNo
				})

				// Save the user
				// Error handling
				newUser.save()
				.then(saved => response.send(true))
				.catch(error => response.send(false))
			}
		})
		.catch(error => response.send(false));
}

// new controller for the authentication of the user
module.exports.loginUser = (request,response) =>{

	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false)
		}else{

			// the compareSync method is used to compare a non encrypted password from the log in form to the encypted password retrieve from the find method. returns true or false depending on the result of the comparison
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password)
			if(isPasswordCorrect){
				return response.send({
					auth : auth.createAccessToken(result)
				})
			}else{
				return response.send(false)
			}

		}
	})
	.catch(error => response.send(false));
}

module.exports.getProfile = (request,response) => {
	const userData = auth.decode(request.headers.authorization);

	// console.log(userData);
	if(userData.isAdmin){
		const id = request.body._id;

		Users.findById(id)
		.then(result => {
			password: result.password = "Confidential"
		return response.send(result);
		})
		.catch(error => response.send(error));
	}else{
		return response.send("You are not an admin, you don't have access")
	}

}

// Controller for the enroll course.
module.exports.enrollCourse = (request, response) =>{

	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false)
	}else{

		// Push to user document
		let isUserUpdated = Users.findOne({_id : userData.id})
		.then(result => {
			result.enrollments.push({
				courseId : courseId
				
			})
			result.save()
			.then(saved => true)
			.catch(error => false)
		})
		.catch(error => false)



		// Push to course document
		let isCourseUpdated = Courses.findOne({
			_id : courseId})
			.then(result => {
				result.enrollees.push({userId : userData.id})

				result.save()
				.then(saved => true)
				.catch(error => false)
		})
		.catch(error => false)

		// If condition to check whether updated the users document and courses document.
		if(isUserUpdated && isCourseUpdated){
			return response.send(true)
		}else{
			return response.send(false)
		}

	}
}

module.exports.retriveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	Users.findById(userData.id)
	.then(data => response.send(data));
}