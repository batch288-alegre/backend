async function getAllToDo(){

   return await (

      fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => response.json())
      .then(json => {
         return json.map(item => item.title)
      })


  );

}
// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       fetch("https://jsonplaceholder.typicode.com/todos/1")
       .then(response => response.json())
       .then(json => json)

   );

}

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

       fetch("https://jsonplaceholder.typicode.com/todos", {
         method : "POST",

         headers:{
                  "Content-Type": "application/json"
         },

         body: JSON.stringify({
               "usedId":1,
               "title": "Create To Do List Item",
               "completed": false
         })
       })
       .then(response => response.json())
       .then(json => json)

   );

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

       fetch("https://jsonplaceholder.typicode.com/todos/1", {
         method : "PUT",

         headers:{
                  "Content-Type": "application/json"
         },

         body: JSON.stringify({
               "dateCompleted": "Pending",
               "description": "To update my to do list with a different data structure",
               "status" : "Pending",
               "title": "Update To Do List Item",
               "usedId": 1
         })
       })
       .then(response => response.json())
       .then(json => json)


   );

}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

       fetch("https://jsonplaceholder.typicode.com/todos/1", {
         method: "DELETE",
       })
       .then(response => response.json())
       .then(json => json)
   );

}




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}
