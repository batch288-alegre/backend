// console.log("Let's Go! MIAMI!")

// [Section] JavaScript Synchronous vs Asynchronous
	// Javascript is by default synchrounous meaning that only one statement is executed at a time.

	// This can be proven when a statement has an error javascript will not proceed with the next statement

/*console.log("Hello World!")

conole.log("Hello after the world")

console.log("Hello")
*/

	// When certain statements take a lot of time to process, this slows down our code.

	// for(let index = 0; index <= 1500; index++){
	// 	console.log(index);
	// }

	// console.log("Hello again")

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

//[Section] Getting all posts
	// The Fetch API allows you to asynchronously request for a ressource data.
	// so it means that fetch method that we are going to use here will run asynchronously.
	// Syntax:
		// fetch("URL");
	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
	console.log(fetch("https://jsonplaceholder.typicode.com/posts"))
	// Syntax:
	/*
		fetch('URL')
		.then((response => response));
	*/

	// Retrieve all posts follow the REST API

	fetch("https://jsonplaceholder.typicode.com/posts")
	// The fetch method will return a promise that will resolves that response object.
	// Use to "json" method from the response object to conver the date retrived into JSON format to be used in our application.
	// the "then" method captures the response object
	.then(response => response.json())
	.then(json => console.log(json))

	// The "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code

	// Creates an asynchrounous function

	async function fetchData(){

		let result = await fetch("https://jsonplaceholder.typicode.com/posts")

		console.log(result)

		let json = await result.json()
		console.log(json)

	};

	// fetchData();

	// [Section] Getting Specific post
	// Retrives specific post following the rest API(/posts/:)

	fetch("https://jsonplaceholder.typicode.com/posts/5")
	.then(response => response.json())
	.then(json => console.log(json))

	// [Section] Creating Post
	// Syntax:
		
			
				/*
				options is an object that contains the method, the headers and the body of the request

				by default if you don't add the method in the fetch request, it will be a GET method.


				fetch("URL, options")
				.then(response => {})
				.then(response => {})
				*/
	fetch("https://jsonplaceholder.typicode.com/posts", {
		// sets the method of the request object to post following thr rest API
		method: "POST",
		// sets the header data of the request object to be sent to the backend
		// sepcified that the content will be in JSON structure.
		headers:{
			"Content-Type": "application/json"
		},
		// sets the content/body data of the request object to be sent to the back end
		body: JSON.stringify({
			title: "New post",
			body: "Hello World",
			userId: 1 
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

	// [Section] Update a specific

	// PUT Method
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method:"PUT",
		headers:{
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			id:1,
			title: "Updated Post",
			body: "Hello again",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

	// PATCH Method
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method:"PATCH",
		headers:{
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Updated Post",
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

	// The PUT method is a method of modifying resource where the clients sends data that updates the entire object/document

	// Patch method applies a partial update to the oject or document

	// deleting specific post following
	fetch("https://jsonplaceholder.typicode.com/posts/1", {method : "DELETE"})
	.then(response => response.json())
	.then(json => console.log(json))
	// .then( json => {
	// 	// map method
	// })