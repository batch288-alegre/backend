// console.log("Good afternoon, Batch 288!");

//Array Methods
	// javaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	//Mutator Methods
	// Mutators methods are functions that "mutate" or change an array after they're created
	//These methods manipulate the original array performing various such as adding or removing elements.

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
		/*
			-Adds an element in the end of an array and returns the updated array's length
			Syntax:
				arrayName.push();
		*/

	console.log("Current array: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');

	console.log(fruitsLength);
	console.log('Mutated array from push method: ');
	console.log(fruits);

	// Pushing multple elements to an array

	fruitsLength = fruits.push('Avocado', 'Guava');

	console.log(fruitsLength);
	console.log("Mutated array after pushing multiple elements:");
	console.log(fruits);


	// pop()
	/*
		-removes the last element AND returns the removed element
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	let removedFruit = fruits.pop();

	console.log(removedFruit);
	console.log("Mutated Array from the pop method:");
	console.log(fruits);

	//unshift()
	/*
		-it adds one or more elements at the beginning of an array AND it returns the update array length
		-Syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB ' . . .)
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruitsLength = fruits.unshift('Lime', 'Banana');

	console.log(fruitsLength);
	console.log("Mutated array from unshift method: ");
	console.log(fruits);

	//shift()
	/*
		-removes an element at the beginning of an array AND returns the removed element.
		-syntax:
			arrayName.shift();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	removedFruit = fruits.shift();
	console.log(removedFruit);
	console.log("Mutated array from the shift method: ");
	console.log(fruits);

	// splice()
	/*
		-Simultaneously removes an elements from a specified index number and adds element
		- Syntax:
			arrayname.splice(startingIndex, deleteCount, elementsToBeAdded)
	*/
	console.log("Current Array: ");
	console.log(fruits);

	fruits.splice(fruits.length, 0, "Cherry");
	console.log("Mutated array after the splice method: ");
	console.log(fruits);

	//sort()
		/*
			Rearranges the array elements in alphanumeric order
				-syntax:
				arrayName.sort();
		*/
	console.log("Current Array: ");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array from the sort method: ");
	console.log(fruits);

	//reverse()
		/*
			-reverses the order of array elements
			-syntax:
				arrayName.reverse();
		*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array from the reverse method: ");
	console.log(fruits);

	// [Section] Non-mutator methods
	/*
		-Non-mutator methods are functions that do not modify or change an array after the're created
		-These methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the output.
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

	// indexOf()
	// returns the index number of the first matching element found in an array.
	// If no match was found, the result will be -1.
	//The search process will be done from the first element proceeding to the last element
	/*
		syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, startingIndex);
	
	*/

	let firstIndex = countries.indexOf('PH');
	console.log(firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log(invalidCountry);

	firstIndex = countries.indexOf('PH', 2);
	console.log(firstIndex);

	console.log(countries);

	// lastIndexOf()
	/*
		-returns the index number of the last matching element found in an array
		- the search process will be done from last element proceeding to the first element
			syntax:
				arrayName.lastIndexOf(searchValue);
				arrayName.lasIndexOf(searchValue, startingIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf('BR');
	console.log(invalidCountry);

	lastIndexOf = countries.lastIndexOf('PH',  	6);
	console.log(lastIndexOf);

	// indexOf, starting from the starting index going to the last element(from left to right);
	//lastIndexOf, starting from the starting index going to the first element(from right to left);

	console.log(countries);

	// slice()
		/*
			-portions/slices elements from an array AND retrun a new array
			-syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
		
		*/

	// Slicing off elements from a specifeid index to the last element.

	let slicedArrayA = countries.slice(2);
	console.log('Result from slice method:');
	console.log(slicedArrayA);

	// Slicing off elements from a specified index to another index:
	// The elements that will be sliced are elements from the starting index until the element before the ending index.
	let slicedArrayB = countries.slice(2, 7);
	console.log('Result from the slice method:');
	console.log(slicedArrayB);

	//slicing off elements starting from the last element of an array:
	let slicedArrayC = countries.slice(-5);
	console.log('Result from the slice method:');
	console.log(slicedArrayC);

	//toString()
	/*
		returns an array as string separated by commas
		syntax:
		arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log('Result from toString method:')
	console.log(stringArray);

	console.log(typeof stringArray);

	// concat()
	// combines arrays to an array or elements and returns the combined result.
	// Syntax:
		// arrayA.concat(arrayB);
		//arrayA.concat(elementA);

	let tasksArrayA = ["drink HTML", "eat javascript"];
	let tasksArrayB = ["inhale CSS", "breathe sass"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	let combinedTasks = tasksArrayA.concat('smell express', 'throw react');

	console.log(combinedTasks);

	// concat multiple array into an array

	let allTasks = tasksArrayA.concat(tasksArrayC, tasksArrayB);
	console.log(allTasks);

	// concat array to an array and element
	let exampleTasks = tasksArrayA.concat(tasksArrayB, 'smell express');
	console.log(exampleTasks);

	//join()
	// returns an array as string separated by speciefied separator string
	// syntax:
		// arrayName.join("separatorString")

	let users = ['John', 'Jane', 'Joe', 'Robert'];

	console.log(users.join());
	console.log(users.join(''));
	console.log(users.join(" - "));


//[Section] Iteration Methods
	// Iteration methods are loops designed to perform repititive task
	// Iteration methods loops over all items in an array


	//forEach()
	// Similar to a for loop that iterates on each of array element.
	// syntax:
		// arrayName.forEach(function(indivElement){statement};)

		console.log(allTasks);
		// ['drink HTML', 'eat javascript', 'get git', 'be node', 'inhale CSS', 'breathe sass']

		allTasks.forEach(function(task){
			console.log(task);
		});

		// filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters.
	let filteredTasks = [];


	allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task);
		}
	});
	console.log(filteredTasks);


	//map()
	// Iterates on each element and returns new array with diffrenet values depending on the result of the function's operation

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
		
		return number * 3;
	})
	console.log(numbers);
	console.log(numberMap);

	// every()
	/*
		it will check if all elements in an array meet the given the condition
		-return true value if all elements meet the condition and false otherwise

		Syntax:
			let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition;

			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let allValid = numbers.every(function(number){
		return (number<6);
	})

	console.log(allValid);

	//some()
	// checks if at least one element in the array meets the given condition.
	/*
		syntax:
		let/const resultArray = arrayName.some(function(indivElement){
			return expression/condtion;
		})

	*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	})

	console.log(someValid);

	//filter()
	// returns new array that contains elements which meets the given
	//returns an empty array if no elements were found
	/*
		Syntax:
			let/const resultArray = arrayName.filter(function(indivElement){
				return expression/condtion;
			})

	*/
	
	numbers = [1, 2, 3, 4, 5];
	
	let filterValid = numbers.filter(function(number){
		return (number % 2 === 0);
	})

	console.log(filterValid);

	// includes()
		// checks uf the argument passed can be found in the array

	let products = ["Mouse", 'Keyboard', 'Laptop', 'Monitor'];

	let productFound1 = products.includes('Mouse');
	console.log(productFound1);

	// reduce()
		// evaluates elements from left to right and returns/reduces the array into single value
	numbers = [1, 2, 3, 4, 5];
	// The first parameter in the function will be accumulator
	//the second parameter in the function will be the currentValue
	let reducedArray = numbers.reduce(function(x, y){
		console.log('Accumulator: ' + x);
		console.log('currentValue: ' + y);
		return x*y;
	})

	console.log(reducedArray);

	 let sales = [190, 12387, 12381283, 1298312, 1928312];


	products = ["Mouse", 'Keyboard', 'Laptop', 'Monitor'];

	let reducedArray = products.reduce(function(x, y){
		return (x+y);
	})

	console.log(reducedArray);

