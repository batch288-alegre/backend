// console.log("Hello World!");

// Conditional Statements allows us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run separate instruction if otherwise.

// [Section] if, else if and else Statement

let numA = -1;

// if statement
	// Executes a statement if the specified condition is met or true.

if(numA < 0){
	console.log('Hello');
}

/*
	Syntax:
		if(condition){
			statement;
		}
*/

// The result of the expression in the if's condition must result to true, else, the statement inside the {} will not run.

console.log(numA<0);

// Let's update the variable and run an  if statement with the same condition;
numA = 0;

if(numA < 0 ){
	console.log("Hello again if numA is 0!");
}

// It will not run because the expression now results to false:
console.log(numA < 0);

// Let's take a look at another example:

let city = "New York";

if( city === "New York"){
	console.log("Welcome to New York City!");
}

console.log(city === "New York");

// else if statement
	/*
		-exectues a statement if previous conditions are false and if the specified condition is true
		-The 'else if' statement is optional and can be added to capture additional conditions to change the flow of a program.
	*/

let numH = 1;

if(numH > 2 ){
	console.log('Hello');
}
else if (numH < 2){
	console.log('World');
}

// We we able to run the else if() statement after we evaluated that the if condition was false.

numH = 2;

if(numH === 2){
	console.log('Hello');
}
else if (numH > 1){
	console.log('World');
}

// else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there.

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");
}
else if (city === "Manila"){
	console.log("Welcome to Manila City, Ph!");
}
else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// Since we failed the condition for the if() and the first else if(), we went to the second else if() and checked.

// else statement
/*
	-executes a statement if all other conditions are false.
	-The "else" statement is optional and can be added to capture any other possible result to change the flow of a program.
*/

let numB = 0;
if(numB > 0 ){
	console.log("Hello from numB!");
}
else if(numB < 0){
	console.log("World from numB!");
}
else{
	console.log('Again from numB');
}

	/*
		Since the preceeding if and else if conditions failed, the else statement was run instead.
	*/

// It will cause an error
/*else{
	console.log("Will not run without an if!");
}*/
/*
else if(numB === 2){
	console.log("This will cause an error!");
}*/


// if, else if and else Statements with functions
	/*
		Most of the times we would like to use if, else if and else statements with functions to control the flow of our program.
	*/

	// We are going to create a function that will tell the Typhoon intensity by providing the wind speed.

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed < 0 ){
			return "Invalid wind speed";
		}
		else if(windSpeed <= 38 && windSpeed >= 0){
			return "Tropical Depression detected!";
		}
		else if(windSpeed >= 39 && windSpeed <= 73){
			return "Tropical Storm detected!";
		}
		else if(windSpeed >= 74 && windSpeed <= 95){
			return "Signal Number 1";
		}
		else if(windSpeed >= 96 && windSpeed <= 110){
			return "Signal Number 2";
		}
		else if(windSpeed >= 11 && windSpeed <= 129){
			return "Signal Number 3";
		}
		else if(windSpeed >= 130 && windSpeed <= 156){
			return "Signal Number 4";
		}
		else {
			return "Signal Number 5";
		}

	}

	console.log(determineTyphoonIntensity(30));

	console.log(determineTyphoonIntensity(-1));
	console.log(determineTyphoonIntensity(157));

	// console.warn() is a good way to print warnings in our console that could hel us developers act on a certain output within our.
	console.warn(determineTyphoonIntensity(40));

// [Section] Truthy and Falsy
	// In JavaScript a truthy value is a value that is considered true when encounted in a boolean context.
	//Falsy values /excemption for truthy:
	/*
		1. false
		2. 0
		3. -0
		4. ""
		5.null
		6. undefined
		7. NaN
	*/

	// Truthy Examples

	if(true){
		console.log("Truthy");
	}

	if(78){ 
		console.log("Truthy")
	}

	// Falsy Examples

	if(false){
		console.log("Falsy");
	}

	if(0 && 78){
		console.log("Falsy");
	}

// [Section] Contional (Ternary) Operator
	/*
	-The Ternary Operator takes in three operands:
		1.condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy

	-it can be used to an if else statement
	-Ternary Operators have an implicit return statement meaning without "return" the resulting expression can be stored in a variable.

		-Syntax:
			condition ? ifTrue : ifFalse;
	
	*/

// Single statement execution
let ternaryResult = (1 < 18) ? true : false;

console.log("Result of Ternary Operator: " + ternaryResult);

let exampleTernary = (0) ? "The number is not equal to zero" : "The number is equal to zero";

console.log(exampleTernary);

// Multiple Statement Execution
function isOfLegalAge(){
	let name = "John"
	return "You are in the legal age limit, " + name;
}

function isUnderAge(){
	let name = "John"
	return "You are under the legal age limit, " + name;
}

// The parseInt() function converts the input received as a string data type into number

// let age = parseInt(prompt("What is your age?"));
// console.log(age);
// console.log(typeof age);
// let legalAge = (age > 18 ) ? isOfLegalAge() : isUnderAge();

// console.log(legalAge);

//[Section] Switch statement
	// Evaluates and expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
	// The break statement is used to terminante the current loop once match has been found.

	/*
		Syntax:
		switch(expression/variable){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/
// The ".toLowerCase() function will change the inpur received from the prompt into all lower case letters"
// let day = prompt("What day of the week is today?").toLowerCase();

// switch(day){
// 	case "monday":
// 		console.log("The color of the day is red!");
// 		break;
// 	case "tuesday":
// 		console.log("The color of the day is orange");
// 		break;
// 	case "wednesday":
// 		console.log("The color of the day is yellow");
// 		break;
// 	case "thursday":
// 		console.log("The color of the day is green");
// 		break;
// 	case "friday":
// 		console.log("The color of the day is blue");
// 		break;
// 	case "saturday":
// 		console.log("The color of the day is indigo");
// 		break;
// 	case "sunday":
// 		console.log("The color of the day is violet");
// 		break;

// 	default:
// 		console.log("Please input a valid day");

// }

// [Section] Try - Catch - Finally Statement
	// try catch statements are commonly used for error handling
	//There are instances when the application returns an error/warning that is not necessarily an error in context of our code.

	function showIntensityAlert(windSpeed){
		try{
			alert(determineTyphoonIntensity(windSpeed))
		}
		// The catch will only run if and only if there was an error in the statement inside our try.
		catch(error){
			console.log(typeof error);

			console.warn(error.message);
		}
		finally{
			// Continue execution of code regardless of success and failure of code execution in the try block.
			alert("Intensity updates will show new alert!");
		}
	}

	showIntensityAlert(100);

	// alerat("Hi");

	console.log("Hi I'm after the showIntensityAlert");