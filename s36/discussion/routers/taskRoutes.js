const express = require("express");

const taskControllers = require("../controllers/taskControllers.js");

// Will contain all the endpoints of our application
const router = express.Router();

router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks)

// Parameterize
// We will create a route using a Delete method at the URL "/tasks/:id"
// The colon here is an identifier that helps to create a dynamic route which allows us to supply information
router.delete("/:id",  taskControllers.deleteTask);
router.get("/:id", taskControllers.getSpecific);
router.put("/:id/complete", taskControllers.updateSpecific);


module.exports = router;