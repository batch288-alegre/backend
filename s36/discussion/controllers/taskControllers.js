const Task = require("../models/task.js");

/*Controllers*/

// This control will get/retrive all the document from the tasks collections

module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// Create a controller that will add data in our database

module.exports.addTasks = (request,response) =>{

	Task.findOne({"name" : request.body.name})
	.then(result => {
		if(result !== null){
			return response.send("Duplicate Task");
		}else{
			let newTask = new Task ({
				"name" : request.body.name
			})
			newTask.save();
			return response.send("New task created!");
		}

	}).catch(error => response.send(error))

}

module.exports.deleteTask = (request,response) => {
	console.log(request.params.id);

	let taskToBeDeleted = request.params.id;

	// In mongoose, we have the findByIdandRemove method that will look for a document with the same id provided from the URL and remove/delete the document from the mongoDB
	Task.findByIdAndRemove(taskToBeDeleted).then(result =>{
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted!`)
	}).catch(error => response.send(error))

}

module.exports.getSpecific = (request, response) => {
	let specificTask = request.params.id;

	Task.findById(specificTask)
	.then(result =>{
		return response.send(result)
	}).catch(error => response.send(error))
}

module.exports.updateSpecific = (request, response) => {
	let specificTask = request.params.id;

	Task.findByIdAndUpdate(specificTask, {"status" : request.body.status}, {new: true})
	.then(result =>{
		response.send(result)
	}).catch(error => response.send(error))
}